import 'package:flutter/material.dart';

main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();

  String _infoText = "informe os seus dados";

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  _resetFields() {
    weightController.clear();
    heightController.clear();
    setState(() {
      _infoText = "informe os seus dados";
      _formKey = GlobalKey<FormState>();
    });
  }

  _calculate() {
    setState(() {
      double weight = double.parse(weightController.text);
      double height = double.parse(heightController.text) / 100;
      double imc = weight / (height * height);
      if (imc < 18.8) {
        _infoText = "abaixo do peso (${imc.toStringAsPrecision(4)})";
      } else if (imc >= 18.6 && imc < 24.9) {
        _infoText = "peso ideal (${imc.toStringAsPrecision(4)})";
      } else if (imc >= 24.9 && imc < 29.9) {
        _infoText = "levemente acima do peso (${imc.toStringAsPrecision(4)})";
      } else if (imc >= 29.9 && imc < 34.9) {
        _infoText =
            "obesidade de primeiro grau (${imc.toStringAsPrecision(4)})";
      } else if (imc >= 34.9 && imc < 39.9) {
        _infoText = "obesidade de segundo grau (${imc.toStringAsPrecision(4)})";
      } else if (imc >= 40.0) {
        _infoText = "obesidade grau três (${imc.toStringAsPrecision(4)})";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Calculadora de IMC"),
          centerTitle: true,
          backgroundColor: Colors.deepPurpleAccent,
          elevation: 4,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: _resetFields,
            )
          ],
        ),
        backgroundColor: Colors.deepPurpleAccent,
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(20.00, 00.00, 20.00, 00.00),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Icon(Icons.person, size: 120.0, color: Colors.white),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: "Peso (kg)",
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 20.0),
                  controller: weightController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "insira o seu peso";
                    }
                  },
                ),
                Padding(padding: EdgeInsets.only(top: 10.00, bottom: 10.00)),
                TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: "Altura (cm)",
                      labelStyle: TextStyle(color: Colors.white),
                    ),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                    controller: heightController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "insira a sua altura!";
                      }
                    }),
                Padding(padding: EdgeInsets.only(top: 10.00, bottom: 10.00)),
                Container(
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _calculate();
                      }
                    },
                    child: Text("Calcular",
                        style: TextStyle(color: Colors.black, fontSize: 20.0)),
                    color: Colors.white,
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 10.00, bottom: 10.00)),
                Text(_infoText,
                    style: TextStyle(color: Colors.white, fontSize: 20.00))
              ],
            ),
          ),
        ));
  }
}
